This patch is based on the upstream commit described below, adjusted to
remove fuzz when applying to the Debian package and to avoid mentioning
F-I-X-M-E (remove the dashes) in the patch which upsets the debcargo
scripts.

commit 728d47506fe3a2599818060bd6e02e4bd3e338d5
Author: Jubilee Young <workingjubilee@gmail.com>
Date:   Sat May 15 20:15:42 2021 -0700

    Update shuffle API for const generics

only in patch2:
--- packed-simd.orig/src/api/shuffle.rs
+++ packed-simd/src/api/shuffle.rs
@@ -81,6 +81,5 @@
         unsafe {
-            $crate::Simd($crate::__shuffle_vector2(
+            $crate::Simd($crate::__shuffle_vector2::<{[$l0, $l1]}, _, _>(
                 $vec0.0,
                 $vec1.0,
-                [$l0, $l1],
             ))
@@ -91,6 +90,5 @@
         unsafe {
-            $crate::Simd($crate::__shuffle_vector4(
+            $crate::Simd($crate::__shuffle_vector4::<{[$l0, $l1, $l2, $l3]}, _, _>(
                 $vec0.0,
                 $vec1.0,
-                [$l0, $l1, $l2, $l3],
             ))
@@ -103,6 +101,5 @@
         unsafe {
-            $crate::Simd($crate::__shuffle_vector8(
+            $crate::Simd($crate::__shuffle_vector8::<{[$l0, $l1, $l2, $l3, $l4, $l5, $l6, $l7]}, _, _>(
                 $vec0.0,
                 $vec1.0,
-                [$l0, $l1, $l2, $l3, $l4, $l5, $l6, $l7],
             ))
@@ -117,5 +114,3 @@
         unsafe {
-            $crate::Simd($crate::__shuffle_vector16(
-                $vec0.0,
-                $vec1.0,
+            $crate::Simd($crate::__shuffle_vector16::<{
                 [
@@ -123,3 +118,6 @@
                     $l11, $l12, $l13, $l14, $l15,
-                ],
+                ]
+            }, _, _>(
+                $vec0.0,
+                $vec1.0,
             ))
@@ -138,5 +136,3 @@
         unsafe {
-            $crate::Simd($crate::__shuffle_vector32(
-                $vec0.0,
-                $vec1.0,
+            $crate::Simd($crate::__shuffle_vector32::<{
                 [
@@ -146,3 +142,6 @@
                     $l29, $l30, $l31,
-                ],
+                ]
+            }, _, _>(
+                $vec0.0,
+                $vec1.0,
             ))
@@ -169,14 +168,13 @@
         unsafe {
-            $crate::Simd($crate::__shuffle_vector64(
+            $crate::Simd($crate::__shuffle_vector64::<{[
+                $l0, $l1, $l2, $l3, $l4, $l5, $l6, $l7, $l8, $l9, $l10,
+                $l11, $l12, $l13, $l14, $l15, $l16, $l17, $l18, $l19,
+                $l20, $l21, $l22, $l23, $l24, $l25, $l26, $l27, $l28,
+                $l29, $l30, $l31, $l32, $l33, $l34, $l35, $l36, $l37,
+                $l38, $l39, $l40, $l41, $l42, $l43, $l44, $l45, $l46,
+                $l47, $l48, $l49, $l50, $l51, $l52, $l53, $l54, $l55,
+                $l56, $l57, $l58, $l59, $l60, $l61, $l62, $l63,
+            ]}, _, _>(
                 $vec0.0,
                 $vec1.0,
-                [
-                    $l0, $l1, $l2, $l3, $l4, $l5, $l6, $l7, $l8, $l9, $l10,
-                    $l11, $l12, $l13, $l14, $l15, $l16, $l17, $l18, $l19,
-                    $l20, $l21, $l22, $l23, $l24, $l25, $l26, $l27, $l28,
-                    $l29, $l30, $l31, $l32, $l33, $l34, $l35, $l36, $l37,
-                    $l38, $l39, $l40, $l41, $l42, $l43, $l44, $l45, $l46,
-                    $l47, $l48, $l49, $l50, $l51, $l52, $l53, $l54, $l55,
-                    $l56, $l57, $l58, $l59, $l60, $l61, $l62, $l63,
-                ],
             ))
only in patch2:
--- packed-simd.orig/src/codegen/llvm.rs
+++ packed-simd/src/codegen/llvm.rs
@@ -8,2 +8,71 @@
 // Shuffle intrinsics: expanded in users' crates, therefore public.
+extern "platform-intrinsic" {
+    pub fn simd_shuffle2<T, U>(x: T, y: T, idx: [u32; 2]) -> U;
+    pub fn simd_shuffle4<T, U>(x: T, y: T, idx: [u32; 4]) -> U;
+    pub fn simd_shuffle8<T, U>(x: T, y: T, idx: [u32; 8]) -> U;
+    pub fn simd_shuffle16<T, U>(x: T, y: T, idx: [u32; 16]) -> U;
+    pub fn simd_shuffle32<T, U>(x: T, y: T, idx: [u32; 32]) -> U;
+    pub fn simd_shuffle64<T, U>(x: T, y: T, idx: [u32; 64]) -> U;
+}
+
+#[allow(clippy::missing_safety_doc)]
+#[inline]
+pub unsafe fn __shuffle_vector2<const IDX: [u32; 2], T, U>(x: T, y: T) -> U
+where
+    T: Simd,
+    <T as Simd>::Element: Shuffle<[u32; 2], Output = U>,
+{
+    simd_shuffle2(x, y, IDX)
+}
+
+#[allow(clippy::missing_safety_doc)]
+#[inline]
+pub unsafe fn __shuffle_vector4<const IDX: [u32; 4], T, U>(x: T, y: T) -> U
+where
+    T: Simd,
+    <T as Simd>::Element: Shuffle<[u32; 4], Output = U>,
+{
+    simd_shuffle4(x, y, IDX)
+}
+
+#[allow(clippy::missing_safety_doc)]
+#[inline]
+pub unsafe fn __shuffle_vector8<const IDX: [u32; 8], T, U>(x: T, y: T) -> U
+where
+    T: Simd,
+    <T as Simd>::Element: Shuffle<[u32; 8], Output = U>,
+{
+    simd_shuffle8(x, y, IDX)
+}
+
+#[allow(clippy::missing_safety_doc)]
+#[inline]
+pub unsafe fn __shuffle_vector16<const IDX: [u32; 16], T, U>(x: T, y: T) -> U
+where
+    T: Simd,
+    <T as Simd>::Element: Shuffle<[u32; 16], Output = U>,
+{
+    simd_shuffle16(x, y, IDX)
+}
+
+#[allow(clippy::missing_safety_doc)]
+#[inline]
+pub unsafe fn __shuffle_vector32<const IDX: [u32; 32], T, U>(x: T, y: T) -> U
+where
+    T: Simd,
+    <T as Simd>::Element: Shuffle<[u32; 32], Output = U>,
+{
+    simd_shuffle32(x, y, IDX)
+}
+
+#[allow(clippy::missing_safety_doc)]
+#[inline]
+pub unsafe fn __shuffle_vector64<const IDX: [u32; 64], T, U>(x: T, y: T) -> U
+where
+    T: Simd,
+    <T as Simd>::Element: Shuffle<[u32; 64], Output = U>,
+{
+    simd_shuffle64(x, y, IDX)
+}
+
 extern "platform-intrinsic" {
@@ -12,47 +81,2 @@
     // https://github.com/rust-lang-nursery/packed_simd/issues/21
-    #[rustc_args_required_const(2)]
-    pub fn simd_shuffle2<T, U>(x: T, y: T, idx: [u32; 2]) -> U
-    where
-        T: Simd,
-        <T as Simd>::Element: Shuffle<[u32; 2], Output = U>;
-
-    #[rustc_args_required_const(2)]
-    pub fn simd_shuffle4<T, U>(x: T, y: T, idx: [u32; 4]) -> U
-    where
-        T: Simd,
-        <T as Simd>::Element: Shuffle<[u32; 4], Output = U>;
-
-    #[rustc_args_required_const(2)]
-    pub fn simd_shuffle8<T, U>(x: T, y: T, idx: [u32; 8]) -> U
-    where
-        T: Simd,
-        <T as Simd>::Element: Shuffle<[u32; 8], Output = U>;
-
-    #[rustc_args_required_const(2)]
-    pub fn simd_shuffle16<T, U>(x: T, y: T, idx: [u32; 16]) -> U
-    where
-        T: Simd,
-        <T as Simd>::Element: Shuffle<[u32; 16], Output = U>;
-
-    #[rustc_args_required_const(2)]
-    pub fn simd_shuffle32<T, U>(x: T, y: T, idx: [u32; 32]) -> U
-    where
-        T: Simd,
-        <T as Simd>::Element: Shuffle<[u32; 32], Output = U>;
-
-    #[rustc_args_required_const(2)]
-    pub fn simd_shuffle64<T, U>(x: T, y: T, idx: [u32; 64]) -> U
-    where
-        T: Simd,
-        <T as Simd>::Element: Shuffle<[u32; 64], Output = U>;
-}
-
-pub use self::simd_shuffle16 as __shuffle_vector16;
-pub use self::simd_shuffle2 as __shuffle_vector2;
-pub use self::simd_shuffle32 as __shuffle_vector32;
-pub use self::simd_shuffle4 as __shuffle_vector4;
-pub use self::simd_shuffle64 as __shuffle_vector64;
-pub use self::simd_shuffle8 as __shuffle_vector8;
-
-extern "platform-intrinsic" {
     crate fn simd_eq<T, U>(x: T, y: T) -> U;
only in patch2:
--- packed-simd.orig/src/lib.rs
+++ packed-simd/src/lib.rs
@@ -202,2 +202,3 @@
 #![feature(
+    const_generics,
     repr_simd,
@@ -217,2 +218,3 @@
         improper_ctypes_definitions,
+        incomplete_features,
         clippy::cast_possible_truncation,
@@ -227,2 +229,3 @@
         clippy::wrong_self_convention,
+        clippy::from_over_into,
 )]
