diff --git a/diesel_derives/src/associations.rs b/diesel_derives/src/associations.rs
index dc1d0a84..14adf08c 100644
--- a/src/associations.rs
+++ b/src/associations.rs
@@ -115,10 +115,10 @@ impl AssociationOptions {
     fn from_meta(meta: MetaItem) -> Result<Self, Diagnostic> {
         let parent_struct = meta
             .nested()?
-            .find(|m| m.word().is_ok() || m.name() == "parent")
+            .find(|m| m.path().is_ok() || m.name().is_ident("parent"))
             .ok_or_else(|| meta.span())
             .and_then(|m| {
-                m.word()
+                m.path()
                     .map(|i| parse_quote!(#i))
                     .or_else(|_| m.ty_value())
                     .map_err(|_| m.span())
@@ -136,18 +136,17 @@ impl AssociationOptions {
                 .path
                 .segments
                 .last()
-                .expect("paths always have at least one segment")
-                .into_value();
+                .expect("paths always have at least one segment");
             meta.nested_item("foreign_key")?
                 .map(|i| i.ident_value())
                 .unwrap_or_else(|| Ok(infer_foreign_key(&parent_struct_name.ident)))?
         };
 
-        let unrecognized_options = meta.nested()?.skip(1).filter(|n| n.name() != "foreign_key");
+        let unrecognized_options = meta.nested()?.skip(1).filter(|n| !n.name().is_ident("foreign_key"));
         for ignored in unrecognized_options {
             ignored
                 .span()
-                .warning(format!("Unrecognized option {}", ignored.name()))
+                .warning(format!("Unrecognized option {}", path_to_string(&ignored.name())))
                 .emit();
         }
 
diff --git a/diesel_derives/src/meta.rs b/diesel_derives/src/meta.rs
index 39830831..8c88efce 100644
--- a/src/meta.rs
+++ b/src/meta.rs
@@ -10,15 +10,19 @@ pub struct MetaItem {
     meta: syn::Meta,
 }
 
+pub(crate) fn path_to_string(path: &syn::Path) -> String {
+    path.segments.iter().map(|s| s.ident.to_string()).collect::<Vec<String>>().join("::")
+}
+
 impl MetaItem {
     pub fn all_with_name(attrs: &[syn::Attribute], name: &str) -> Vec<Self> {
         attrs
             .iter()
             .filter_map(|attr| {
-                attr.interpret_meta()
+                attr.parse_meta().ok()
                     .map(|m| FixSpan(attr.pound_token.spans[0]).fold_meta(m))
             })
-            .filter(|m| m.name() == name)
+            .filter(|m| m.path().is_ident(name))
             .map(|meta| Self { meta })
             .collect()
     }
@@ -30,7 +34,7 @@ impl MetaItem {
     pub fn empty(name: &str) -> Self {
         Self {
             meta: syn::Meta::List(syn::MetaList {
-                ident: Ident::new(name, Span::call_site()),
+                path: syn::Path::from(Ident::new(name, Span::call_site())),
                 paren_token: Default::default(),
                 nested: Default::default(),
             }),
@@ -38,7 +42,7 @@ impl MetaItem {
     }
 
     pub fn nested_item(&self, name: &str) -> Result<Option<Self>, Diagnostic> {
-        self.nested().map(|mut i| i.find(|n| n.name() == name))
+        self.nested().map(|mut i| i.find(|n| n.name().is_ident(name)))
     }
 
     pub fn required_nested_item(&self, name: &str) -> Result<Self, Diagnostic> {
@@ -56,7 +60,7 @@ impl MetaItem {
                 self.span()
                     .error(format!(
                         "`{0}` must be in the form `{0} = \"true\"`",
-                        self.name()
+                        path_to_string(&self.name())
                     ))
                     .emit();
                 false
@@ -67,22 +71,22 @@ impl MetaItem {
     pub fn expect_ident_value(&self) -> syn::Ident {
         self.ident_value().unwrap_or_else(|e| {
             e.emit();
-            self.name()
+            self.name().segments.first().unwrap().ident.clone()
         })
     }
 
     pub fn ident_value(&self) -> Result<syn::Ident, Diagnostic> {
         let maybe_attr = self.nested().ok().and_then(|mut n| n.nth(0));
-        let maybe_word = maybe_attr.as_ref().and_then(|m| m.word().ok());
-        match maybe_word {
+        let maybe_path = maybe_attr.as_ref().and_then(|m| m.path().ok());
+        match maybe_path {
             Some(x) => {
                 self.span()
                     .warning(format!(
                         "The form `{0}(value)` is deprecated. Use `{0} = \"value\"` instead",
-                        self.name(),
+                        path_to_string(&self.name()),
                     ))
                     .emit();
-                Ok(x)
+                Ok(x.segments.first().unwrap().ident.clone())
             }
             _ => Ok(syn::Ident::new(
                 &self.str_value()?,
@@ -91,23 +95,23 @@ impl MetaItem {
         }
     }
 
-    pub fn expect_word(&self) -> syn::Ident {
-        self.word().unwrap_or_else(|e| {
+    pub fn expect_path(&self) -> syn::Path {
+        self.path().unwrap_or_else(|e| {
             e.emit();
             self.name()
         })
     }
 
-    pub fn word(&self) -> Result<syn::Ident, Diagnostic> {
+    pub fn path(&self) -> Result<syn::Path, Diagnostic> {
         use syn::Meta::*;
 
         match self.meta {
-            Word(ref x) => Ok(x.clone()),
+            Path(ref x) => Ok(x.clone()),
             _ => {
                 let meta = &self.meta;
                 Err(self.span().error(format!(
                     "Expected `{}` found `{}`",
-                    self.name(),
+                    path_to_string(&self.name()),
                     quote!(#meta)
                 )))
             }
@@ -121,19 +125,19 @@ impl MetaItem {
             List(ref list) => Ok(Nested(list.nested.iter())),
             _ => Err(self
                 .span()
-                .error(format!("`{0}` must be in the form `{0}(...)`", self.name()))),
+                .error(format!("`{0}` must be in the form `{0}(...)`", path_to_string(&self.name())))),
         }
     }
 
-    pub fn name(&self) -> syn::Ident {
-        self.meta.name()
+    pub fn name(&self) -> syn::Path {
+        self.meta.path().clone()
     }
 
     pub fn has_flag(&self, flag: &str) -> bool {
         self.nested()
             .map(|mut n| {
-                n.any(|m| match m.word() {
-                    Ok(word) => word == flag,
+                n.any(|m| match m.path() {
+                    Ok(word) => word.is_ident(flag),
                     Err(_) => false,
                 })
             })
@@ -152,7 +156,7 @@ impl MetaItem {
     pub fn expect_str_value(&self) -> String {
         self.str_value().unwrap_or_else(|e| {
             e.emit();
-            self.name().to_string()
+            path_to_string(&self.name())
         })
     }
 
@@ -167,7 +171,7 @@ impl MetaItem {
             Str(ref s) => Ok(s),
             _ => Err(self.span().error(format!(
                 "`{0}` must be in the form `{0} = \"value\"`",
-                self.name()
+                path_to_string(&self.name())
             ))),
         }
     }
@@ -183,7 +187,7 @@ impl MetaItem {
 
         match *self.lit_value()? {
             Str(ref s) => s.value().parse().map_err(|_| error),
-            Int(ref i) => Ok(i.value()),
+            Int(ref i) => i.base10_parse().map_err(|_| error),
             _ => Err(error),
         }
     }
@@ -195,7 +199,7 @@ impl MetaItem {
             NameValue(ref name_value) => Ok(&name_value.lit),
             _ => Err(self.span().error(format!(
                 "`{0}` must be in the form `{0} = \"value\"`",
-                self.name()
+                path_to_string(&self.name())
             ))),
         }
     }
@@ -205,11 +209,11 @@ impl MetaItem {
             Ok(x) => x,
             Err(_) => return,
         };
-        let unrecognized_options = nested.filter(|n| !options.iter().any(|&o| n.name() == o));
+        let unrecognized_options = nested.filter(|n| !options.iter().any(|&o| n.name().is_ident(o)));
         for ignored in unrecognized_options {
             ignored
                 .span()
-                .warning(format!("Option {} has no effect", ignored.name()))
+                .warning(format!("Option {} has no effect", path_to_string(&ignored.name())))
                 .emit();
         }
     }
@@ -218,7 +222,7 @@ impl MetaItem {
         use syn::Meta::*;
 
         match self.meta {
-            Word(ref ident) => ident.span(),
+            Path(ref path) => path.span(),
             List(ref meta) => meta.nested.span(),
             NameValue(ref meta) => meta.lit.span(),
         }
diff --git a/diesel_derives/src/model.rs b/diesel_derives/src/model.rs
index 25722a81..2d862f11 100644
--- a/src/model.rs
+++ b/src/model.rs
@@ -18,7 +18,7 @@ impl Model {
         let table_name_from_attribute =
             MetaItem::with_name(&item.attrs, "table_name").map(|m| m.expect_ident_value());
         let primary_key_names = MetaItem::with_name(&item.attrs, "primary_key")
-            .map(|m| Ok(m.nested()?.map(|m| m.expect_word()).collect()))
+            .map(|m| Ok(m.nested()?.map(|m| m.expect_path().segments.first().unwrap().ident.clone()).collect()))
             .unwrap_or_else(|| Ok(vec![Ident::new("id", Span::call_site())]))?;
         let fields = fields_from_item_data(&item.data)?;
         Ok(Self {
